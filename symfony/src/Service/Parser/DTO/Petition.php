<?php
declare(strict_types=1);

namespace App\Service\Parser\DTO;

class Petition
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $link;
    /**
     * @var string
     */
    private $description;
    /**
     * @var int
     */
    private $petitionID;
    /**
     * @var int
     */
    private $signatureCount;
    /**
     * @var string
     */
    private $wideImage;
    /**
     * @var string
     */
    private $freeformSponsor;
    /**
     * @var string
     */
    private $freeformTarget;
    /**
     * @var \DateTime
     */
    private $stopdate;
    /**
     * @var string
     */
    private $summary;
    /**
     * @var string
     */
    private $status;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Petition
     */
    public function setTitle(string $title): Petition
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return Petition
     */
    public function setLink(string $link): Petition
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Petition
     */
    public function setDescription(string $description): Petition
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getPetitionID(): int
    {
        return $this->petitionID;
    }

    /**
     * @param int $petitionID
     * @return Petition
     */
    public function setPetitionID(int $petitionID): Petition
    {
        $this->petitionID = $petitionID;
        return $this;
    }

    /**
     * @return int
     */
    public function getSignatureCount(): int
    {
        return $this->signatureCount;
    }

    /**
     * @param int $signatureCount
     * @return Petition
     */
    public function setSignatureCount(int $signatureCount): Petition
    {
        $this->signatureCount = $signatureCount;
        return $this;
    }

    /**
     * @return string
     */
    public function getWideImage(): string
    {
        return $this->wideImage;
    }

    /**
     * @param string $wideImage
     * @return Petition
     */
    public function setWideImage(string $wideImage): Petition
    {
        $this->wideImage = $wideImage;
        return $this;
    }

    /**
     * @return string
     */
    public function getFreeformSponsor(): string
    {
        return $this->freeformSponsor;
    }

    /**
     * @param string $freeformSponsor
     * @return Petition
     */
    public function setFreeformSponsor(string $freeformSponsor): Petition
    {
        $this->freeformSponsor = $freeformSponsor;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStopdate(): \DateTime
    {
        return $this->stopdate;
    }

    /**
     * @param \DateTime $stopdate
     * @return Petition
     */
    public function setStopdate(\DateTime $stopdate): Petition
    {
        $this->stopdate = $stopdate;
        return $this;
    }

    /**
     * @return string
     */
    public function getSummary(): string
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     * @return Petition
     */
    public function setSummary(string $summary): Petition
    {
        $this->summary = $summary;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Petition
     */
    public function setStatus(string $status): Petition
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getFreeformTarget(): string
    {
        return $this->freeformTarget;
    }

    /**
     * @param string $freeformTarget
     * @return Petition
     */
    public function setFreeformTarget(string $freeformTarget): Petition
    {
        $this->freeformTarget = $freeformTarget;
        return $this;
    }
}