<?php
declare(strict_types=1);

namespace App\Service\Parser\Downloader;

use App\Service\Parser\DTO\Petition;

interface DownloaderInterface
{
    /**
     * @return Petition[]
     */
    public function download(): array;
}