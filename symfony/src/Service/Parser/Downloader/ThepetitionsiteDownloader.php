<?php
declare(strict_types=1);

namespace App\Service\Parser\Downloader;

use App\Service\Parser\Converter\ConverterInterface;
use App\Service\Parser\DTO\Petition;
use Symfony\Component\HttpClient\HttpClient;

class ThepetitionsiteDownloader implements DownloaderInterface
{
    /**
     * @var ConverterInterface
     */
    private $converter;
    /**
     * @var string
     */
    private $source;
    /**
     * @var string
     */
    private $section;

    public function __construct(ConverterInterface $converter, string $source, string $section)
    {
        $this->converter = $converter;
        $this->source = $source;
        $this->section = $section;
    }

    /**
     * @return Petition[]
     */
    public function download(): array
    {
        $result = [];
        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', $this->source);

        foreach (\json_decode($response->getContent(), true)[$this->section] as $petition) {
            $result[] = $this->converter->convert($petition);
        }

        return $result;
    }
}