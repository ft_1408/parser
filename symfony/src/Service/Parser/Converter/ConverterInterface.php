<?php
declare(strict_types=1);

namespace App\Service\Parser\Converter;

use App\Service\Parser\DTO\Petition;

interface ConverterInterface
{
    public function convert(array $item): Petition;
}