<?php
declare(strict_types=1);

namespace App\Service\Parser\Converter;

use App\Service\Parser\DTO\Petition;

class PetitionsConverter implements ConverterInterface
{

    public function convert(array $item): Petition
    {
        $petition = new Petition();
        $petition->setTitle($item['title']);
        $petition->setLink($item['link']);
        $petition->setDescription($item['description']);
        $petition->setPetitionID((int) $item['petitionID']);
        $petition->setSignatureCount((int) $item['signature_count']);
        $petition->setWideImage($item['wide_image']);
        $petition->setFreeformSponsor($item['freeform_sponsor']);
        $petition->setFreeformTarget((string) $item['freeform_target']);
        $petition->setStopdate(new \DateTime($item['stopdate']));
        $petition->setSummary($item['summary']);
        $petition->setStatus($item['status']);

        return $petition;
    }
}