<?php
declare(strict_types=1);

namespace App\Service\Parser\Types;

class PetitionProviderType
{
    public const THEPETITIONSITE = 'thepetitionsite';
}