<?php
declare(strict_types=1);

namespace App\Service\Petitions;

use App\Entity\Petition;
use App\Form\Type\ShowPetitionsType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class PetitionsSerializer
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function serialize(FormInterface $filterForm): string
    {
        $petitions = $this->em->getRepository(Petition::class)->findBy([], ['stopdate' => 'DESC']);

        $encoder = new JsonEncoder();
        $serializer = new Serializer([new DateTimeNormalizer(), new ObjectNormalizer()], [$encoder]);

        if ($filterForm->getData() === null) {
            return $serializer->serialize($petitions, 'json', ['ignored_attributes' => ShowPetitionsType::EXCLUDED_PROPERTIES]);
        }

        $serializableKeys = array_keys(array_filter($filterForm->getData(), function($v, $k) {
            return $v === true;
        },ARRAY_FILTER_USE_BOTH));

        return $serializer->serialize($petitions, 'json', ['attributes' => $serializableKeys]);
    }
}