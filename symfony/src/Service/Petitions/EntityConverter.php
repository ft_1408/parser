<?php
declare(strict_types=1);

namespace App\Service\Petitions;

use App\Service\Parser\DTO\Petition;
use App\Entity\Petition as EntityPetition;
use Doctrine\ORM\EntityManagerInterface;

class EntityConverter
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Petition[] $petitions
     */
    public function convertAndSave(array $petitions): void
    {
        foreach ($petitions as $petition) {
            $entity = $this->toEntity($petition);
            $this->em->persist($entity);
        }

        $this->em->flush();
    }

    private function toEntity(Petition $petition): EntityPetition
    {
        $entityPetition = $this->em->
            getRepository(EntityPetition::class)
            ->findOneBy(['internalId' => $petition->getPetitionID()]) ?? new EntityPetition();
        $entityPetition->setTitle($petition->getTitle());
        $entityPetition->setLink($petition->getLink());
        $entityPetition->setDescription($petition->getDescription());
        $entityPetition->setInternalId($petition->getPetitionID());
        $entityPetition->setSignatureCount($petition->getSignatureCount());
        $entityPetition->setWideImage($petition->getWideImage());
        $entityPetition->setFreeformSponsor($petition->getFreeformSponsor());
        $entityPetition->setFreeformTarget($petition->getFreeformTarget());
        $entityPetition->setStopdate($petition->getStopdate());
        $entityPetition->setSummary($petition->getSummary());
        $entityPetition->setStatus($petition->getStatus());

        return $entityPetition;
    }
}