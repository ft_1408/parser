<?php
declare(strict_types=1);

namespace App\Command;

use App\Service\Parser\Downloader\DownloaderInterface;
use App\Service\Petitions\EntityConverter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ServiceLocator;

class LoadPetitionsCommand extends Command
{
    protected static $defaultName = 'load:petitions';
    /**
     * @var ServiceLocator
     */
    private $downloaderLocator;
    /**
     * @var EntityConverter
     */
    private $petitionsEntityConverter;

    public function __construct(ServiceLocator $downloaderLocator, EntityConverter $petitionsEntityConverter)
    {
        parent::__construct();
        $this->downloaderLocator = $downloaderLocator;
        $this->petitionsEntityConverter = $petitionsEntityConverter;
    }

    protected function configure()
    {
        $this
            ->setDescription('Parses petitions from selected source')
            ->addOption('source', 's', InputOption::VALUE_REQUIRED, 'Source for parsing petitions')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $source = $input->getOption('source');

        try {
            if (!$this->downloaderLocator->has($source)) {
                throw new \InvalidArgumentException(\sprintf('Unavailable source type "%s"', $source));
            }

            /**
             * @var $downloader DownloaderInterface
             */
            $downloader = $this->downloaderLocator->get($source);
            $petitions = $downloader->download();
            $this->petitionsEntityConverter->convertAndSave($petitions);

        } catch (\Throwable $e) {
            $error = \sprintf('%s in %s on %s', $e->getMessage(), $e->getFile(), $e->getLine());
            $output->writeln($error);

            return false;
        }

        return true;
    }
}