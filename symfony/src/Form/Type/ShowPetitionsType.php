<?php
declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\Petition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class ShowPetitionsType extends AbstractType
{
    public const EXCLUDED_PROPERTIES = ['id'];

    /**
     * @param FormBuilderInterface $builder
     * @param mixed[]              $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $reflect = new \ReflectionClass(new Petition());
        $props = $reflect->getProperties(\ReflectionProperty::IS_PRIVATE);

        foreach ($props as $prop) {
            if (in_array($prop->getName(), self::EXCLUDED_PROPERTIES)) {
                continue;
            }

            $builder->add($prop->getName(), CheckboxType::class, [
                'required' => false,
                'attr' => ['checked' => 'checked']
            ]);
        }

        $builder->add('filter', SubmitType::class, ['label' => 'Filter']);
    }
}