<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Petition {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $link;

    /**
     * @ORM\Column(type="text")
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $internalId;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $signatureCount;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $wideImage;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $freeformSponsor;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $freeformTarget;

    /**
     * @ORM\Column(type="date")
     *
     * @var \DateTime
     */
    private $stopdate;

    /**
     * @ORM\Column(type="text")
     *
     * @var string
     */
    private $summary;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $status;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Petition
     */
    public function setTitle(string $title): Petition
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return Petition
     */
    public function setLink(string $link): Petition
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Petition
     */
    public function setDescription(string $description): Petition
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getInternalId(): int
    {
        return $this->internalId;
    }

    /**
     * @param int $internalId
     * @return Petition
     */
    public function setInternalId(int $internalId): Petition
    {
        $this->internalId = $internalId;
        return $this;
    }

    /**
     * @return int
     */
    public function getSignatureCount(): int
    {
        return $this->signatureCount;
    }

    /**
     * @param int $signatureCount
     * @return Petition
     */
    public function setSignatureCount(int $signatureCount): Petition
    {
        $this->signatureCount = $signatureCount;
        return $this;
    }

    /**
     * @return string
     */
    public function getWideImage(): string
    {
        return $this->wideImage;
    }

    /**
     * @param string $wideImage
     * @return Petition
     */
    public function setWideImage(string $wideImage): Petition
    {
        $this->wideImage = $wideImage;
        return $this;
    }

    /**
     * @return string
     */
    public function getFreeformSponsor(): string
    {
        return $this->freeformSponsor;
    }

    /**
     * @param string $freeformSponsor
     * @return Petition
     */
    public function setFreeformSponsor(string $freeformSponsor): Petition
    {
        $this->freeformSponsor = $freeformSponsor;
        return $this;
    }

    /**
     * @return string
     */
    public function getFreeformTarget(): string
    {
        return $this->freeformTarget;
    }

    /**
     * @param string $freeformTarget
     * @return Petition
     */
    public function setFreeformTarget(string $freeformTarget): Petition
    {
        $this->freeformTarget = $freeformTarget;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStopdate(): \DateTime
    {
        return $this->stopdate;
    }

    /**
     * @param \DateTime $stopdate
     * @return Petition
     */
    public function setStopdate(\DateTime $stopdate): Petition
    {
        $this->stopdate = $stopdate;
        return $this;
    }

    /**
     * @return string
     */
    public function getSummary(): string
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     * @return Petition
     */
    public function setSummary(string $summary): Petition
    {
        $this->summary = $summary;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Petition
     */
    public function setStatus(string $status): Petition
    {
        $this->status = $status;
        return $this;
    }
}