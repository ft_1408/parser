<?php
declare(strict_types=1);

namespace App\Controller;

use App\Form\Type\ShowPetitionsType;
use App\Service\Petitions\PetitionsSerializer;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/petition");
 */
class PetitionsController extends AbstractController
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var PetitionsSerializer
     */
    private $petitionsSerializer;

    public function __construct(FormFactoryInterface $formFactory, PetitionsSerializer $petitionsSerializer)
    {
        $this->formFactory = $formFactory;
        $this->petitionsSerializer = $petitionsSerializer;
    }

    /**
     * @Route("/", name="show_petitions", methods={"GET","POST"})
     */
    public function showPetitions(Request $request): Response
    {
        $form = $this->formFactory->createNamed('', ShowPetitionsType::class);
        $form->handleRequest($request);

        return $this->render('petitions/showPetitions.twig',
            [
                'petitions' => $this->petitionsSerializer->serialize($form),
                'form' => $form->createView(),
            ]
        );
    }
}