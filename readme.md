**Requirements**

- Docker
- Docker compose

**Installation**

 - Add symfony.localhost in your `/etc/hosts` file
 - Run:
 ```bash
 $ docker-compose up
 ```

You are done, you can visit your Symfony application on the following URL: http://symfony.localhost (and access Kibana on http://symfony.localhost:81)

Based on: `https://github.com/eko/docker-symfony`